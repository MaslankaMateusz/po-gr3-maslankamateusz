package pl.edu.uwm.wmii.maslankamateusz.labolatorium08;


import java.util.Arrays;


public class TestOsoba
{
    public static void main(String[] args)
    {
        Osoba[] ludzie = new Osoba[2];

        ludzie[0] = new Pracownik("Boniek", "Zbigi", "Stasiu", 1997, 05, 22, 2001, 2, 4, false, 4200.25);
        ludzie[1] = new Student("Donald", "Jaroslaw", 1943, 02, 14, true, "Informatyka", 3.0);

        for (Osoba p : ludzie) {
            System.out.println(p.getNazwisko() + " "+ Arrays.deepToString(p.getimiona()) + " : " + p.getOpis() + " ,Data urodzenia: " + p.getdataUrodzenia() + " ,Płeć: " + p.getpłeć());
        }
    }
}



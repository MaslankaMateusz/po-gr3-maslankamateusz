package pl.edu.uwm.wmii.maslankamateusz.labolatorium01;
import java.util.*;
public class Zadanie1_1a {

    public static void main(String[] args) {
        Scanner InputValue = new Scanner(System.in);
        System.out.println("Podaj ile liczb: ");
        int n = InputValue.nextInt();
        int suma = 0;

        for (int i = 0; i < n; i++) {
            System.out.println("Podaj następną wartość: ");
            int a = InputValue.nextInt();
            suma += a;
        }
        System.out.print(suma);
    }
}
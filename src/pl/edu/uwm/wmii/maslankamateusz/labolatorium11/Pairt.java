package pl.edu.uwm.wmii.maslankamateusz.labolatorium11;

public class Pairt<T> {

    public Pairt() {
        first = null;
        second = null;
    }

    public Pairt (T first, T second) {
        this.first = first;
        this.second = second;
    }

    public T getFirst() {
        return first;
    }
    public T getSecond() {
        return second;
    }

    public void setFirst (T newValue) {
        first = newValue;
    }
    public void setSecond (T newValue) {
        second = newValue;
    }

    public void swap () {
        T tmp = first;
        first = second;
        second = tmp;
    }

    private T first;
    private T second;
}


package pl.edu.uwm.wmii.maslankamateusz.labolatorium00;

public class Zadanie9 {
    public static void main(String[] args) {
        System.out.println(
                "     /\\_/\\         -----\n" +
                        "    ( O O )      / Hello \\\n" +
                        " <==(  v  )==>  <  Young  |\n" +
                        "     | | |       \\ Coder!/\n" +
                        "    <__|__>        -----");
    }
}

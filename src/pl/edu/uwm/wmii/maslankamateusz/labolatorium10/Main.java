package pl.edu.uwm.wmii.maslankamateusz.labolatorium10;


import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.io.*;

public class Main {

    public static void main(String[] args) throws Exception  {
        Osoba obj1 = new Osoba("Lujek",1997,2,14);
        Osoba obj3 = new Osoba("Kujek",1993,9,11);
        Osoba obj2 = new Osoba("Mujek",1994,01,25);
        System.out.println(obj1.toString());
        System.out.println(obj2.toString());
        System.out.println(obj1.equals(obj1));
        System.out.println(obj1.equals(obj2));
        System.out.println(obj1.compareTo(obj1));
        System.out.println(obj1.compareTo(obj2));
        System.out.println(obj1.compareTo(obj3));

        List<Osoba> lista = new ArrayList<Osoba>();
        Osoba l5 = new Osoba("DDDDDD",1997,9,9);
        Osoba l1 = new Osoba("AAACS",1998,11,12);
        Osoba l3 = new Osoba("CCSAC",1992,11,12);
        Osoba l4 = new Osoba("DDDFD",1992,11,12);
        Osoba l2 = new Osoba("AAAFSD",1997,11,12);
        Osoba l6 = new Osoba("AAjSGA",1996,11,12);
        Osoba l7 = new Osoba("AAAG",1992,2,12);
        Osoba l8 = new Osoba("AAASSG",1992,3,12);
        Osoba l9 = new Osoba("AAAF",1992,2,2);
        Osoba l10 = new Osoba("AAAFAC",1992,3,1);

        Osoba s1 = new Student("AAA",1992,3,1,4.33);
        Osoba s2 = new Student("AAA",1992,3,2,4.00);
        Osoba s3 = new Student("AAA",1992,4,1,4.00);
        Osoba s4 = new Student("AAA",1993,3,1,4.00);
        Osoba s5 = new Student("AAB",1992,3,2,4.00);
        Osoba s6 = new Student("AAA",1992,3,2,4.11);
        Osoba s7 = new Student("AAA",1992,3,2,4.01);

        lista.add(s1);
        lista.add(s2);
        lista.add(s3);
        lista.add(s4);
        lista.add(s5);
        lista.add(s6);
        lista.add(s7);


        for(Osoba o : lista) {
            System.out.println(o.toString());
        }
        System.out.println("-------------------------------");

        Collections.sort(lista);

        for(Osoba o : lista) {
            System.out.println(o.toString());
        }
        System.out.println("-------------------------------");


        // read from file

        File file = new File("C:\\Users\\ABC\\IdeaProjects\\po-gr1-maslankamateusz\\src\\pl\\edu\\uwm\\wmii\\maslankamateusz\\labolatorium10\\test.txt");

        BufferedReader br = new BufferedReader(new FileReader(file));
        List<String> Stringi = new ArrayList<String>();

        String st;
        while ((st = br.readLine()) != null)
            Stringi.add(st);

        Collections.sort(Stringi);

        for(String x : Stringi) {
            System.out.println(x);
        }
        System.out.println("-------------------------------");

    }
}
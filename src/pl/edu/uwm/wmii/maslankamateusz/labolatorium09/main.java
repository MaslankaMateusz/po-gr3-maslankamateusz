package pl.edu.uwm.wmii.maslankamateusz.labolatorium09;

import java.time.LocalDate;
import java.time.Month;
import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        Osoba o = new Osoba("Bartek", LocalDate.of(1995, Month.AUGUST, 23));
        Osoba o2 = new Osoba("Michal", LocalDate.of(1995, Month.SEPTEMBER, 13));

        int x = o2.ileLat();
        int y = o2.ileMiesiecy();
        int z = o2.ileDni();
        System.out.println("-----------");
        System.out.println("Lat: "+x + " Miesiecy: " +  Math.abs(y) + " Dni: " + Math.abs(z));
        System.out.println("-----------");
        System.out.println(o.equals(o2));
        System.out.println(o.toString());
        System.out.println(o.compareTo(o2));
        test();
    }

    public static void test(){
        Osoba[] grupa = new Osoba[5];

        grupa[0] = new Osoba("Tusk", LocalDate.of(1948,05,5));
        grupa[1] = new Osoba("Kaczynski", LocalDate.of(1949,1,21));
        grupa[4] = new Osoba("Knut", LocalDate.of(1994,04,31));
        grupa[3] = new Osoba("Michalski", LocalDate.of(1993,9,03));
        grupa[2] = new Osoba("Bizancjum", LocalDate.of(1981,1,21));


        for (int i = 0; i < grupa.length; i++){
            System.out.println(grupa[i].toString() + "\t\tLat: " + grupa[i].ileLat() + ", Miesiecy: " + Math.abs(grupa[i].ileMiesiecy()) + ", Dni: " +  Math.abs(grupa[i].ileDni()));
        }
        Arrays.sort(grupa);
        System.out.println("===============================================");

        for (int i = 0; i < grupa.length; i++){
            System.out.println(grupa[i].toString() + "\t\tLat: " + grupa[i].ileLat() + ", Miesiecy: " + Math.abs(grupa[i].ileMiesiecy()) + ", Dni: " + Math.abs(grupa[i].ileDni()));
        }
    }
}
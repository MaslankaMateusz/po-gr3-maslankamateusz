package pl.edu.uwm.wmii.maslankamateusz.labolatorium00;

public class Zadanie11 {
    public static void main(String[] args) {
        System.out.println("Mróz\n" +
                "Tu leży staroświecka jak przecinek\n" +
                "autorka paru wierszy. Wieczny odpoczynek\n" +
                "raczyła dać jej ziemia, pomimo że trup\n" +
                "nie należał do żadnej z literackich grup.\n" +
                "Ale też nic lepszego nie ma na mogile\n" +
                "oprócz tej rymowanki, łopianu i sowy. P\n" +
                "Przechodniu, wyjmij z teczki mózg elektronowy\n" +
                "i nad losem Szymborskiej podumaj przez chwilę.");
    }
}

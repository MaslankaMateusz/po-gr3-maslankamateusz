package pl.edu.uwm.wmii.maslankamateusz.labolatorium01;


import java.util.Scanner;
import static java.lang.Math.pow;

public class Zadanie1_1i {

    public static void main(String[] args) {
        Scanner InputValue = new Scanner(System.in);
        System.out.println("Podaj ile liczb: ");
        int n = InputValue.nextInt();
        int suma = 0;
        int silnia = 1;

        for(int i = 1; i <= n; i++) {
            System.out.println("Podaj następną wartość: ");
            int a = InputValue.nextInt();
            silnia *= i;
            suma += (a*pow((-1),i))/silnia;
        }
        System.out.print(suma);
    }
}
